# Pre-requesites


1. Make sure you have Python 3 and PIP 3 installed.


2. Execute below command to check (open terminal / cmd prompt)  :

  - `python --version`

  - `pip --version`


# Run the python script ( all of this happens in the same terminal )

1. Install virtualenv

  - `pip install virtualenv`


2. In (terminal / cmd prompt),  goto the place where you have downloaded / cloned this folder.

3. When you are at tpot-prabhu folder location, execute below commands :

  - `virtualenv environment`

  - `environment\Scripts\activate`

4. Once you see environment being active :

  - `pip install -r req.txt`


5. Goto   `code\tutorials` folder, and execute :

  - `python Titanic_Kaggle.py`


6. You can run jupyter notebook from same place :

  - `jupyter notebook`

# Run Random Forest or Ada Boost as script

  - Activate virtualenv

  - `cd tpot-prabu/code/tutorials`

  - For Running AdaBoost

  - `python AdaBoost.py data/train.csv data/test.csv`

  - For Running Random Forest

  - `python RandomForest.py data/train.csv data/test.csv`
