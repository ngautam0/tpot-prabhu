#importing all the required ML packages
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import GridSearchCV


import warnings
import argparse
import os
import numpy as np
import multiprocessing
import pprint

from helpers.clean_n_split import feature_engineer
from helpers.graphs import generate_roc, generate_lift

parser = argparse.ArgumentParser(description='challenger model and metrics building')
parser.add_argument('x',metavar='xvar',type=str, help='x vars csv file')
parser.add_argument('y',metavar='yvar',type=str, help='y var csv file')
parser.add_argument('output',nargs='?', default=os.getcwd(), help='fire policy pif count assigned to the agent')
#parser.add_argument('model',nargs='?',help='benchmark model pickle file')
parser.add_argument('cv',nargs='?',type = int,default = 5, help='benchmark model pickle file')
args = parser.parse_args()


def main(args):
    warnings.filterwarnings('ignore')

    X_train, X_val, y_train, y_val, X_test = feature_engineer(args.x, args.y)

    num_workers = multiprocessing.cpu_count()

    print('Running Grid Seach on AdaBoostClassifier')

    #grid search on adaboost - 10 fold cv
    # Hyperparameter search for adaboosting
    n_estimators=list(range(100,1100,100))
    learn_rate=[0.05,0.2,0.4,0.6,0.8]
    hyper={'n_estimators':n_estimators,'learning_rate':learn_rate}
    gd=GridSearchCV(estimator=AdaBoostClassifier(),param_grid=hyper,verbose=True, n_jobs=num_workers, cv=10)

    print('***********************************************************')
    print('\t\t Params')
    print('***********************************************************')
    pprint.pprint(hyper)

    print('***********************************************************')
    print('\t\t Training')
    print('***********************************************************')
    gd.fit(X_train,y_train)

    print('***********************************************************')
    print('Best Score - ', gd.best_score_)
    print('Best Estimator', gd.best_estimator_)
    print('***********************************************************')


    # # mean & std dev
    means = gd.cv_results_['mean_test_score']
    stds = gd.cv_results_['std_test_score']
    params = gd.cv_results_['params']

    print('***********************************************************')
    print('\t\t Mean & Standard dev')
    print('***********************************************************')
    
    for mean, std, params in zip(means, stds, params):
            print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))


    # roc and lift
    generate_roc(gd, X_train, y_train, 'image/ada/train_roc.png')
    generate_roc(gd, X_val, y_val, 'image/ada/val_roc.png')
    generate_lift(gd, X_train, y_train, 'image/ada/train_lift.png')
    generate_lift(gd, X_val, y_val,'image/ada/val_lift.png')

    print('***********************************************************')
    print('Saved ROC and Lift chart - image/ada folder')
    print('***********************************************************')

if __name__ == '__main__':
    main(args)
