#importing all the required ML packages
from sklearn.ensemble import RandomForestClassifier #Random Forest
from sklearn.model_selection import GridSearchCV


import warnings
import argparse
import os
import numpy as np
import multiprocessing
import pprint

from helpers.clean_n_split import feature_engineer
from helpers.graphs import generate_roc, generate_lift

parser = argparse.ArgumentParser(description='challenger model and metrics building')
parser.add_argument('x',metavar='xvar',type=str, help='x vars csv file')
parser.add_argument('y',metavar='yvar',type=str, help='y var csv file')
parser.add_argument('output',nargs='?', default=os.getcwd(), help='fire policy pif count assigned to the agent')
#parser.add_argument('model',nargs='?',help='benchmark model pickle file')
parser.add_argument('cv',nargs='?',type = int,default = 5, help='benchmark model pickle file')
args = parser.parse_args()


def main(args):
    warnings.filterwarnings('ignore')

    X_train, X_val, y_train, y_val, X_test = feature_engineer(args.x, args.y)

    num_workers = multiprocessing.cpu_count()

    print('Running Grid Seach on Random Forest')

    # Hyperparameter search for random forest
    n_estimators=list(range(100,1100,100))
    hyper={'n_estimators':n_estimators}
    rfgd=GridSearchCV(estimator=RandomForestClassifier(random_state = 0),param_grid=hyper,verbose=True, cv=10)

    print('***********************************************************')
    print('\t\t Params')
    print('***********************************************************')
    pprint.pprint(hyper)

    print('***********************************************************')
    print('\t\t Training')
    print('***********************************************************')
    rfgd.fit(X_train,y_train)

    print('***********************************************************')
    print('Best Score - ', rfgd.best_score_)
    print('Best Estimator', rfgd.best_estimator_)
    print('***********************************************************')


    # # mean & std dev
    means = rfgd.cv_results_['mean_test_score']
    stds = rfgd.cv_results_['std_test_score']
    params = rfgd.cv_results_['params']

    print('***********************************************************')
    print('\t\t Mean & Standard dev')
    print('***********************************************************')
    
    for mean, std, params in zip(means, stds, params):
            print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))


    # roc and lift
    generate_roc(rfgd, X_train, y_train, 'image/rf/train_roc.png')
    generate_roc(rfgd, X_val, y_val, 'image/rf/val_roc.png')
    generate_lift(rfgd, X_train, y_train, 'image/rf/train_lift.png')
    generate_lift(rfgd, X_val, y_val,'image/rf/val_lift.png')
    print('***********************************************************')
    print('Saved ROC and Lift chart - image/rf folder')
    print('***********************************************************')

if __name__ == '__main__':
    main(args)
