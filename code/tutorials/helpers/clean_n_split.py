# pandas
import pandas as pd
from pandas import Series,DataFrame

# numpy
import numpy as np

# sklearn train_test_split
from sklearn.model_selection import train_test_split #training and testing data split

def get_person(passenger):
    age,sex = passenger
    return 'child' if age < 16 else sex

def feature_engineer(train_path, predict_path):
    titanic_df = pd.read_csv( train_path)
    test_df    = pd.read_csv(predict_path)

    # drop unnecessary columns, these columns won't be useful in analysis and prediction
    titanic_df = titanic_df.drop(['PassengerId','Name','Ticket'], axis=1)
    test_df    = test_df.drop(['Name','Ticket'], axis=1)
    titanic_df.drop(['Embarked'], axis=1,inplace=True)
    test_df.drop(['Embarked'], axis=1,inplace=True)
    # Cabin
    # It has a lot of NaN values, so it won't cause a remarkable impact on prediction
    titanic_df.drop("Cabin",axis=1,inplace=True)
    test_df.drop("Cabin",axis=1,inplace=True)

    # Family
    # Instead of having two columns Parch & SibSp,
    # we can have only one column represent if the passenger had any family member aboard or not,
    # Meaning, if having any family member(whether parent, brother, ...etc) will increase chances of Survival or not.
    titanic_df['Family'] =  titanic_df["Parch"] + titanic_df["SibSp"]
    titanic_df['Family'].loc[titanic_df['Family'] > 0] = 1
    titanic_df['Family'].loc[titanic_df['Family'] == 0] = 0

    test_df['Family'] =  test_df["Parch"] + test_df["SibSp"]
    test_df['Family'].loc[test_df['Family'] > 0] = 1
    test_df['Family'].loc[test_df['Family'] == 0] = 0

    # drop Parch & SibSp
    titanic_df = titanic_df.drop(['SibSp','Parch'], axis=1)
    test_df    = test_df.drop(['SibSp','Parch'], axis=1)

    # Sex

    # As we see, children(age < ~16) on aboard seem to have a high chances for Survival.
    # So, we can classify passengers as males, females, and child
    titanic_df['Person'] = titanic_df[['Age','Sex']].apply(get_person,axis=1)
    test_df['Person']    = test_df[['Age','Sex']].apply(get_person,axis=1)

    # No need to use Sex column since we created Person column
    titanic_df.drop(['Sex'],axis=1,inplace=True)
    test_df.drop(['Sex'],axis=1,inplace=True)

    # create dummy variables for Person column, & drop Male as it has the lowest average of survived passengers
    person_dummies_titanic  = pd.get_dummies(titanic_df['Person'])
    person_dummies_titanic.columns = ['Child','Female','Male']
    person_dummies_titanic.drop(['Male'], axis=1, inplace=True)

    person_dummies_test  = pd.get_dummies(test_df['Person'])
    person_dummies_test.columns = ['Child','Female','Male']
    person_dummies_test.drop(['Male'], axis=1, inplace=True)

    titanic_df = titanic_df.join(person_dummies_titanic)
    test_df    = test_df.join(person_dummies_test)

    titanic_df.drop(['Person'],axis=1,inplace=True)
    test_df.drop(['Person'],axis=1,inplace=True)


    # only for test_df, since there is a missing "Fare" values
    test_df["Fare"].fillna(test_df["Fare"].median(), inplace=True)

    # convert from float to int
    titanic_df['Fare'] = titanic_df['Fare'].astype(int)
    test_df['Fare']    = test_df['Fare'].astype(int)

    # get average, std, and number of NaN values in titanic_df
    average_age_titanic   = titanic_df["Age"].mean()
    std_age_titanic       = titanic_df["Age"].std()
    count_nan_age_titanic = titanic_df["Age"].isnull().sum()

    # get average, std, and number of NaN values in test_df
    average_age_test   = test_df["Age"].mean()
    std_age_test       = test_df["Age"].std()
    count_nan_age_test = test_df["Age"].isnull().sum()

    # generate random numbers between (mean - std) & (mean + std)
    rand_1 = np.random.randint(average_age_titanic - std_age_titanic, average_age_titanic + std_age_titanic, size = count_nan_age_titanic)
    rand_2 = np.random.randint(average_age_test - std_age_test, average_age_test + std_age_test, size = count_nan_age_test)

    # fill NaN values in Age column with random values generated
    titanic_df["Age"][np.isnan(titanic_df["Age"])] = rand_1
    test_df["Age"][np.isnan(test_df["Age"])] = rand_2

    # convert from float to int
    titanic_df['Age'] = titanic_df['Age'].astype(int)
    test_df['Age']    = test_df['Age'].astype(int)

    # create dummy variables for Pclass column, & drop 3rd class as it has the lowest average of survived passengers
    pclass_dummies_titanic  = pd.get_dummies(titanic_df['Pclass'])
    pclass_dummies_titanic.columns = ['Class_1','Class_2','Class_3']
    pclass_dummies_titanic.drop(['Class_3'], axis=1, inplace=True)

    pclass_dummies_test  = pd.get_dummies(test_df['Pclass'])
    pclass_dummies_test.columns = ['Class_1','Class_2','Class_3']
    pclass_dummies_test.drop(['Class_3'], axis=1, inplace=True)

    titanic_df.drop(['Pclass'],axis=1,inplace=True)
    test_df.drop(['Pclass'],axis=1,inplace=True)

    titanic_df = titanic_df.join(pclass_dummies_titanic)
    test_df    = test_df.join(pclass_dummies_test)


    # define training and testing sets
    X_train = titanic_df.drop("Survived",axis=1)
    Y_train = titanic_df["Survived"]
    X_test  = test_df.drop("PassengerId",axis=1).copy()

    # Create train and validation set out of Train data
    X_train, X_val, y_train, y_val = train_test_split(X_train, Y_train, test_size=0.3, random_state=0)

    return (X_train, X_val, y_train, y_val, X_test)
