import matplotlib
matplotlib.use('agg')

import matplotlib.pyplot as plt
import scikitplot as skplt
from sklearn.metrics import roc_curve, auc

def generate_roc(model, X, y, save_path):

    prediction = model.predict(X)
    fpr, tpr, thresholds = roc_curve(prediction, y)

    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr, color='darkorange', label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.05])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig(save_path)

def generate_lift(model, X, y, save_path):
    prediction_prob = model.predict_proba(X)
    skplt.metrics.plot_cumulative_gain(y, prediction_prob)

    plt.xlim([0.0, 1.05])
    plt.ylim([0.0, 1.05])
    plt.savefig(save_path)
